// +build cgo

package gisconversions

import (
	"fmt"
	"github.com/ctessum/gogeom/geom/encoding/wkt"
	"github.com/dhconnelly/rtreego"
	"github.com/lukeroth/gdal"
	"github.com/paulsmith/gogeos/geos"
	"github.com/twpayne/gogeom/geom"
	"github.com/twpayne/gogeom/geom/encoding/wkb"
	"strconv"
	"strings"
)

// Calculate the rtreego bounding box of a GDAL envelope.
func GDALToRect(e gdal.Envelope) (bounds *rtreego.Rect, err error) {
	xmin := e.MinX()
	xmax := e.MaxX()
	ymin := e.MinY()
	ymax := e.MaxY()
	p := rtreego.Point{xmin, ymin}
	lengths := []float64{xmax - xmin, ymax - ymin}
	if lengths[0] == 0. {
		lengths[0] = 1.e-6
	}
	if lengths[1] == 0. {
		lengths[1] = 1.e-6
	}
	bounds, err = rtreego.NewRect(p, lengths)
	if err != nil {
		fmt.Println(p, lengths)
		return
	}
	return
}

// Calculate the rtreego bounding box of a geom shape.
func GeomToRect(g geom.T) (bounds *rtreego.Rect, err error) {
	b := geom.NewBounds()
	b = g.Bounds(b)
	p := rtreego.Point{b.Min.X, b.Min.Y}
	lengths := []float64{b.Max.X - b.Min.X,
		b.Max.Y - b.Min.Y}
	if lengths[0] == 0. {
		lengths[0] = 1.e-6
	}
	if lengths[1] == 0. {
		lengths[1] = 1.e-6
	}
	bounds, err = rtreego.NewRect(p, lengths)
	if err != nil {
		fmt.Println(p, lengths)
		return
	}
	return
}

// Calculate the rtreego bounding box of a GEOS shape.
func GeosToRect(g *geos.Geometry) (bounds *rtreego.Rect, err error) {
	var WKT string
	var envelope *geos.Geometry
	envelope, err = g.Envelope()
	if err != nil {
		return
	}
	WKT, err = envelope.ToWKT()
	if err != nil {
		return
	}
	WKTTrimmed := strings.Trim(WKT, "POLYGONMULTILINESTRINGPOINT() ")
	WKTSplit := strings.Split(WKTTrimmed, ",")
	var x, y, xmin, ymin, xmax, ymax float64
	for i, coord := range WKTSplit {
		xy := strings.Split(strings.TrimSpace(coord), " ")
		x, err = strconv.ParseFloat(xy[0], 64)
		if err != nil {
			return
		}
		y, err = strconv.ParseFloat(xy[1], 64)
		if err != nil {
			return
		}
		if i == 0 {
			xmin, xmax = x, x
			ymin, ymax = y, y
		} else {
			if x < xmin {
				xmin = x
			}
			if x > xmax {
				xmax = x
			}
			if y < ymin {
				ymin = y
			}
			if y > ymax {
				ymax = y
			}
		}
	}
	p := rtreego.Point{xmin, ymin}
	lengths := []float64{xmax - xmin, ymax - ymin}
	if lengths[0] == 0. {
		lengths[0] = 1.e-6
	}
	if lengths[1] == 0. {
		lengths[1] = 1.e-6
	}
	bounds, err = rtreego.NewRect(p, lengths)
	if err != nil {
		fmt.Println(p, lengths)
		return
	}
	return
}

func GDALtoGEOS(g gdal.Geometry) (g2 *geos.Geometry, err error) {
	var WKT string
	WKT, err = g.ToWKT()
	if err.Error() != "No Error" {
		return
	} else {
		err = nil
	}
	g2, err = geos.FromWKT(WKT)
	return
}

func GeomToGEOS(g geom.T) (g2 *geos.Geometry, err error) {
	var WKT []byte
	if g == nil {
		return
	}
	WKT, err = wkt.Encode(g)
	if err != nil {
		return
	}
	g2, err = geos.FromWKT(string(WKT))
	return
}
func GEOStoGeom(g *geos.Geometry) (g2 geom.T, err error) {
	var WKB []byte
	var WKT string
	if g == nil {
		return
	}
	WKT, err = g.ToWKT()
	if err != nil {
		return
	}
	if strings.Contains(WKT, "GEOMETRYCOLLECTION EMPTY") {
		return
	}
	WKB, err = g.WKB()
	if err != nil {
		return
	}
	g2, err = wkb.Decode(WKB)
	return
}
func GeomToGDAL(g geom.T, sr gdal.SpatialReference) (
	g2 gdal.Geometry, err error) {
	var WKT []byte
	if g == nil {
		return
	}
	WKT, err = wkt.Encode(g)
	if err != nil {
		return
	}
	g2, err = gdal.CreateFromWKT(string(WKT), sr)
	if err.Error() != "No Error" {
		return
	} else {
		err = nil
	}
	return
}

func GEOStoGDAL(g *geos.Geometry, sr gdal.SpatialReference) (
	g2 gdal.Geometry, err error) {
	var WKT string
	WKT, err = g.ToWKT()
	if err != nil {
		return
	}
	g2, err = gdal.CreateFromWKT(WKT, sr)
	if err.Error() != "No Error" {
		return
	} else {
		err = nil
	}
	return
}

// Convert a GEOS geometry to a GDAL geometry, transform it to a
// different spatial projection, and then convert it back to GEOS.
// The end result is that g is transformed in place.
// Beware: this function apparently causes a memory leak.
func GeosTransform(g *geos.Geometry, initialSr gdal.SpatialReference,
	ct gdal.CoordinateTransform) (result *geos.Geometry, err error) {
	g2, err := GEOStoGDAL(g, initialSr)
	if err != nil {
		return
	}
	err = g2.Transform(ct)
	if err.Error() != "No Error" {
		return
	}
	result, err = GDALtoGEOS(g2)
	if err != nil {
		return
	}
	g2.Destroy()
	return
}

